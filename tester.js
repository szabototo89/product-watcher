const Auchan = require('./src/scrapers/auchan-scraper');

(async function() {
  console.log('Job has started.');

  for await (const product of Auchan.getAllProducts()) {
    console.log(`inserting ${product.name}`);
  }

  console.log('Job has finished.');
})();
