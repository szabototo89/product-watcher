const express = require('express');
const { transports, format } = require('winston');
const winstonMiddleware = require('express-winston');

const { getAllProducts, getMongoClient } = require('../repository');

const port = 3000;

const application = express();
application.use(express.json());
application.use(
  winstonMiddleware.logger({
    transports: [new transports.Console()],
    format: format.combine(
      format.colorize(),
      format.timestamp(),
      format.prettyPrint()
    )
  })
);

const router = express.Router();

router.get('/api/product', async (request, response) => {
  const limit = Number(request.query.limit || 100);
  const products = await getAllProducts(await getMongoClient());

  return response.json({
    result: {
      limit
    },
    products: await products.limit(limit).toArray()
  });
});

application.use(router);

application.listen(port, () => {
  console.log(`Server is listening on ${port}.`);
});
