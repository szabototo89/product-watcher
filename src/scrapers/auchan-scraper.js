const axios = require('axios');
const cheerio = require('cheerio');
const { getPageContent, parsePrice, parseUnitPrice } = require('./utils');

const baseUrl = 'https://online.auchan.hu';

function getFullUrl(url) {
  return `${baseUrl}${url}`;
}

/*
  return {
    name,
    regularPrice,
    unitPrice,
    url: detailedProductUrl,
    prices: {
      regular: parsePrice(regularPrice),
      unit: parseUnitPrice(unitPrice)
    }
  };
*/

async function getProductCategories(url) {
  const $ = await getPageContent(url);

  return $('.category-block')
    .get()
    .map($)
    .map(el => el.attr('href'))
    .map(getFullUrl)
    .map(url => `${url}?itemsPerPage=10000`);
}

function getProductData(productCard) {
  const productName = productCard.find('a[data-product-name]');
  const name = productName.data('product-name');
  const url = getFullUrl(productName.attr('href'));
  const regularPrice = productCard.find('.current-price > .price').text();
  const unitPrice = productCard
    .find('.unit-price')
    .text()
    .trim();

  return {
    name,
    url,
    regularPrice,
    unitPrice,
    prices: {
      regular: parsePrice(regularPrice),
      unit: parseUnitPrice(unitPrice)
    }
  };
}

async function* getAllProducts() {
  for (const productCategory of await getProductCategories(
    'https://online.auchan.hu/shop/elelmiszer'
  )) {
    const subProductCategories = await getProductCategories(productCategory);

    for (const categoryUrl of subProductCategories) {
      const $ = await getPageContent(categoryUrl);

      const productCards = $('.product_card');
      const products = productCards
        .get()
        .map($)
        .map(getProductData);

      for (const product of products) {
        yield product;
      }
    }
  }
}

module.exports = {
  getAllProducts
};
