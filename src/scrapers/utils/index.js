const axios = require('axios');
const cheerio = require('cheerio');

async function getPageContent(url) {
  const { data: content } = await axios.get(url);
  return cheerio.load(content);
}

function parseUnitPrice(price) {
  const [pricePerUnit, unit] = price.split('/');
  return {
    ...parsePrice(pricePerUnit),
    unit: (unit || '').trim()
  };
}

function parsePrice(price) {
  const [amountAsStr, currency] = [
    price.substring(0, price.length - 3),
    price.substring(price.length - 3).trim()
  ];
  const amount = Number(amountAsStr.replace(/\s*/gi, ''));

  return {
    amount,
    currency
  };
}

module.exports = { getPageContent, parsePrice, parseUnitPrice };
