const axios = require('axios');
const cheerio = require('cheerio');
const { getPageContent, parsePrice, parseUnitPrice } = require('./utils');

async function* getProductPageContents(url) {
  function getNextUrl(selector) {
    return selector('.next-link > .next').attr('href');
  }

  async function* getPageSelectors(url) {
    let currentUrl = url;

    do {
      const selector = await getPageContent(currentUrl);
      yield selector;
      currentUrl = getNextUrl(selector);
    } while (currentUrl != null);
  }

  for await (const selector of getPageSelectors(url)) {
    yield* getProducts(selector);
  }
}

function getProducts(selector) {
  const selectProduct = selector =>
    selector('.category-products .product-grid-item');
  const selectProductName = selector => selector.find('.product-name > a');

  const getProductName = selector => selector.text().trim();
  const getRegularPrice = selector =>
    selector
      .find('.regular-price')
      .text()
      .trim();

  const getNormalUnitPrice = selector =>
    selector
      .find('.unit-price-normal')
      .text()
      .trim();

  const getDetailedProductUrl = selector =>
    selectProductName(selector)
      .attr('href')
      .trim();

  return selectProduct(selector)
    .map((index, element) => {
      const selectElement = selector(element);

      const name = getProductName(selectProductName(selectElement));
      const regularPrice = getRegularPrice(selectElement);
      const unitPrice = getNormalUnitPrice(selectElement);
      const detailedProductUrl = getDetailedProductUrl(selectElement);

      return {
        name,
        regularPrice,
        unitPrice,
        url: detailedProductUrl,
        prices: {
          regular: parsePrice(regularPrice),
          unit: parseUnitPrice(unitPrice)
        }
      };
    })
    .get();
}

async function getProductCategories() {
  const selector = await getPageContent('https://shop.rossmann.hu/');
  const sidebarNavMenu = selector('#sidebar-nav-menu');
  const categoryAnchors = sidebarNavMenu.find(
    'li.level0 > a.collapsible-wrapper'
  );
  const categoryUrls = categoryAnchors
    .map((index, el) => selector(el).attr('href'))
    .get();

  return categoryUrls;
}

async function* getAllProducts() {
  const productCategoryUrls = await getProductCategories();

  for (const productCategoryUrl of productCategoryUrls) {
    yield* getProductPageContents(productCategoryUrl);
  }
}

module.exports = {
  getAllProducts
};
