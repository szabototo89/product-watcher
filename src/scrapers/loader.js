const { getMongoClient, insertProduct } = require('../repository');

const args = require('minimist')(process.argv.slice(2));

(async function() {
  const { name } = args;
  const Scraper = require(`./${name}`);
  const client = await getMongoClient();

  const productInsertions = [];

  console.log(`Job (${name}) has started.`);

  for await (const product of Scraper.getAllProducts()) {
    console.log(`inserting ${product.name}`);
    productInsertions.push(insertProduct(client, product));
  }

  await Promise.all(productInsertions);

  console.log('Job has finished.');
  client.close();
})();
