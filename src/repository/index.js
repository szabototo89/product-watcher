const MongoClient = require('mongodb').MongoClient;

const PRODUCT_COLLECTION_NAME = 'rossmann';

async function getMongoClient() {
  return await MongoClient.connect('mongodb://localhost:27017', {
    auth: { user: 'root', password: 'example' }
  });
}

async function getAllProducts(client) {
  const db = await client.db(PRODUCT_COLLECTION_NAME);
  const productCollection = await db.collection('products');

  return await productCollection.find({});
}

async function insertProduct(
  client,
  { name, regularPrice, unitPrice, url, prices }
) {
  const db = await client.db(PRODUCT_COLLECTION_NAME);
  const productCollection = await db.collection('products');

  return await productCollection.insertOne({
    name,
    regularPrice,
    unitPrice,
    url,
    prices
  });
}

module.exports = {
  getMongoClient,
  getAllProducts,
  insertProduct
};
